console.log("JsScript");

//object is like a car/person
//properties defines the car/person (height,name,weight)
//methods are the things the object can do (run, walk, brake)

/*
A data type that is used to represent real world objects
- Object literals{}
-Information are stored in key:value pair
- Key = properties

In JS, most core JS features like Strings and arrays are objects
- Arrays are collection of data
- Strings are collection of characters

- Different data types maybe stored in an object's property creating complex data structure
*/

let gradesArray = [98, 94, 89, 90];

let grades = {
	firstGrading: 98,
	secondGrading: 94,
	thirdGrading: 89,
	fourthGrading: 90
}
console.log(grades);

//this, means in this 
let user = {
	firstName: 'John',
	lastName: 'Doe',
	age: 25,
	location:{
		city: 'Tokyo',
		country: 'Japan'
	},
	emails:[
		'john@mail.com',
		'johndoe@mail.com'
	],
	fullName: function(){
		return this.firstName + "" + this.lastName;
	}
};
console.log(user);
console.log(user.fullName());

//Creating Objects
//1. using objects initializers/literal notation
/*This creates/declares an object and also initializes/assigns its properties upon creation. A cellphone is an example of a real world object. It has its own properties such as name, color, weight, unit model, etc.

Syntax:
	let objectName ={
		keyA:ValueA,
		KeyB:ValueB
	}

*/

let cellphone ={
		keyA: 'Nokia 3210',
		manufactureDate:1999
	};

console.log('Result from using initializers');
console.log(cellphone);
console.log(typeof cellphone);


//2. using object constructor function
/*
Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object

Note: We always need a NEW in a new object to signify JS that it is a new object
Syntax:
	function ObjectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB;
	}

	let newObject = new ObjectName(keyA, keyB)
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

let laptop = new Laptop('Lenovo', 2008);
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log(myLaptop);


//Creating empty object
let computer = {};
let myComputer = new Object();


//Accessing Object Properties (2 ways)
//if it is an object of an object, you can call it: friend.address.city
//1. using the dot notation
console.log('Result from dot notation:' +myLaptop.name);

//2. using the square bracket notation
console.log('Result from square bracket:' +myLaptop['name']);

let array = [laptop, myLaptop, myLaptop];
console.log(array[0]['name']);
console.log(array[0].name); //easier

//Initializing object properties
let car = {};

car.name = "Honda Civic";
console.log('Result from adding dot notation');
console.log(car)

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log('Resut from using square bracket notation');
console.log(car);


//Deleting Object Properties
delete car['manufacture date'];
console.log('Result from deleting properties:');
console.log(car);

//Reassigning properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties:');
console.log(car.name);

//Object Methods
/*A method is a function which is a property of an object
Similar to functions/features of a real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is' + this.name);
	}
};

console.log(person);
console.log('Result from object method');
person.talk(); //talk is an method of an object

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.');
}
person.walk();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address:{
		city: 'Austin',
		country: 'Texas',
	},
	emails: [
		'joe@email.com',
		'joesmith@mail.com'
	],
	introduce: function(){
		console.log('Hello my name is '+this.firstName+ ' '+this.lastName);
	}
}

friend.introduce();
console.log(friend.address.city);

/*Real world application of objects
- Scenario
1. We would like to create a game that would have several pokemon interact with each other
2. Every pokemon would have the same set of stats, properties, and functions.
*/
//Using object literals to create multiple kinds of pokemon would be time consuming

//internal: Please review this part
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled targetPokemon');
	},
	faint: function(){
		console.log("Pokemon fainted.");
	}
}

console.log(myPokemon);

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' +target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonhealth_");
	},
	this.faint = function(){
		console.log(this.name+ ' fainted.');
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
//internal: Please review this part